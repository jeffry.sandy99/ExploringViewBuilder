//
//  ExploringViewBuilderApp.swift
//  ExploringViewBuilder
//
//  Created by Jeffry Sandy Purnomo on 30/03/21.
//

import SwiftUI

@main
struct ExploringViewBuilderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
