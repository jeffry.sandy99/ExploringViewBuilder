//
//  ContentView.swift
//  ExploringViewBuilder
//
//  Created by Jeffry Sandy Purnomo on 30/03/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView{
            VStack{
                AlertView {
                    Image(systemName: "exclamationmark.shield.fill")
                        .resizable()
                        .frame(width: 65, height: 65)
                    Text("Here is the custom alert with ViewBuilder!")
                }
            }
            .navigationTitle("View Builders")
        }
    }
}

struct AlertView<Content: View>: View{
    let content: Content
    
    init(@ViewBuilder content: ()-> Content) {
        self.content = content()
    }
    var body: some View{
        VStack{
            content
                .padding()
            
            Divider()
            
            HStack{
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                    Text("Cancel")
                        .bold()
                        .foregroundColor(.red)
                })
                .padding(.trailing, 10)
                Button(action: /*@START_MENU_TOKEN@*/{}/*@END_MENU_TOKEN@*/, label: {
                    Text("Confirm")
                        .bold()
                        .foregroundColor(.white)
                })
                .padding(.leading, 10)
            }
        }
        .frame(width: UIScreen.main.bounds.size.width/2)
        .background(Color.blue)
        .cornerRadius(7)
        .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
